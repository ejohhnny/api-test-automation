# Rest Api Test Automation Project

Rest Api test automation project for the petstore api

# Project Configuration

## Set Up

Install node.js preferably `v12.16.1` or higher

## Clone the repo

SSH `git clone git@gitlab.com:ejohhnny/api-test-automation.git` 

HTTP `git clone https://gitlab.com/ejohhnny/api-test-automation.git`

## Install the dependencies

Move to the root of the project structure and install the dependencies

`npm install`

# Execute the test cases

## Execute the test cases manually

`npm run test`

## Execute the test cases in Pipeline

go to CI/CD section, select pipelines, click en run pipeline, select branch and run

# Calliopre test link

 [Calliope report link](https://app.calliope.pro/reports/109036)

# Scenarios

- post a pet
- get a pet
- update a pet
- delete a pet

# Why are the scenarios important?

Theose are the basci  scenarios of the pet service 

# Next Steps

Add test cases for the rest of the api, add  automatic generated data