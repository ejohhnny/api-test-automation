import { assert } from 'chai';
import petRequest from './Pet'
import { Pet } from "./Convert";
import testPet from "./testdata/pet.json"
import updatePet from "./testdata/updatePet.json"
import { Message } from "./ConvertMessage";

describe('Pets Rest API', () => {
    it('POST Pet', async () => {
      let pet:Pet = await petRequest.postAPet(testPet)
      assert.equal(pet.id,2)
      assert.equal(pet.category.id, 2)
      assert.equal(pet.category.name, "Pastores")
      assert.equal(pet.name, "Pastor Aleman")
      assert.equal(pet.tags[0].id, 0)
      assert.equal(pet.tags[0].name, "perro grande")
      assert.equal(pet.status, "available")
    })

    it('GET Pet', async () => {
      let pet:Pet = await petRequest.getPet(2)
      assert.equal(pet.id,2)
      assert.equal(pet.category.id, 2)
      assert.equal(pet.category.name, "Pastores")
      assert.equal(pet.name, "Pastor Aleman")
      assert.equal(pet.tags[0].id, 0)
      assert.equal(pet.tags[0].name, "perro grande")
      assert.equal(pet.status, "available")
    })

    it('PUT Pet', async () => {
      let pet:Pet = await petRequest.updatePet(updatePet)
      assert.equal(pet.id,2)
      assert.equal(pet.category.id, 2)
      assert.equal(pet.category.name, "Shepherd")
      assert.equal(pet.name, "German Shepherd")
      assert.equal(pet.tags[0].id, 0)
      assert.equal(pet.tags[0].name, "Big Dog")
      assert.equal(pet.status, "available")
    })

    it('DELETE Pet', async () => {
      let message:Message = await petRequest.deletePet(2);
      assert.equal(message.code,200)
      assert.equal(message.type, "unknown")
      assert.equal(message.message, "2")

    })

})