import  request from "supertest";
import endpoints from "./testdata/endpoints.json";
import { Convert } from "./Convert";
import { Transform } from "./ConvertMessage";

class PetRequest {


    constructor() {

    }

    async postAPet(pet:any){
        let res = await request(endpoints.base)
                                .post('/pet')
                                .send(pet)
                                .expect(200);
        return Convert.toPet(res.text);
    }

    async getPet(id:number){
        let res = await request(endpoints.base)
                                .get('/pet/'+id)
                                .expect(200);
        return Convert.toPet(res.text);
    }

    async updatePet(pet:any){
        let res = await request(endpoints.base)
                                .put('/pet')
                                .send(pet)
                                .expect(200);
        return Convert.toPet(res.text);
    }

    async deletePet(id:number){
        let res = await request(endpoints.base)
                                .delete('/pet/'+id)
                                .set('api_key','special-key')
                                .expect(200);
        return Transform.toMessage(res.text);
    }

}

export default new PetRequest();